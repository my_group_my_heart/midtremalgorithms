/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm;

import java.util.Scanner;

/**
 *
 * @author 66986
 */
public class midtermII {

    public static void main(String args[]) {

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("string want to convert int:");
            String inputString = scanner.next();
            System.out.println("convert string to int :");
            System.out.println(inputString);
            int intOutput = convertStringToInt(inputString);
            System.out.println("Plus 1 value to improve :");
            System.out.println(intOutput + 1);
        } catch (Exception e) {
            System.out.println(e.getMessage());

        }

    }

    public static int convertStringToInt(String inputString) {
        int i = 0;
        int num = 0;
        boolean Negative = false;
        int len = inputString.length();

        if (inputString.charAt(0) == '-') {
            Negative = true;
            i = 1;
        }

        while (i < len) {
            num = num * 10;
            num += (inputString.charAt(i++) - '0');
        }

        if (Negative) {
            num = -num;
        }
        return num;
    }

}
